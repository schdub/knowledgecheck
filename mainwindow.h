#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>

#include <QJsonObject>
#include <QJsonDocument>

enum ClientStatus {
    statusLogin,
    statusTestPick,
    statusTesting,
    statusResult,
    statusFail
};

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onAuth(QString login, QString pass);
    void onAnswer();
    void onSelected(int index);

    void onPrev();
    void onNext();

    void readyRead();
    void displayError(QAbstractSocket::SocketError socketError);
    void slotDisconnect();
    void slotOnEndTimer();
    void slotQuestionClicked(int index);

protected:
    void closeEvent(QCloseEvent *event);

private:
    QVector<QJsonArray> mAnswers;
    QJsonDocument mQuestions;
    int mCurrentQuestion;
    int mTotalQuestions;

    QTcpSocket mSocket;
    ClientStatus mStatus;
    QByteArray mBuff;

    QTimer * mTimer;

    int mTestDuration;
    bool mTesting;
    bool mConnected;

    QByteArray mCredentials;

    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
