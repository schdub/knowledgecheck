#include "barswidget.h"
#include <QPainter>

BarsWidget::BarsWidget(QWidget *parent) : QWidget(parent)
{}

void BarsWidget::paintEvent(QPaintEvent *event) {
    Q_UNUSED(event);

    QPainter p(this);
    const float barWidth = 50;
    const float barHeight = 345;

    int total = mCorrect + mUncorrect;
    const float oneBarHeight = barHeight / total;

    p.setPen(Qt::black);
    p.setBrush(QBrush(Qt::white));

    p.drawLine(55, 0, 55, height());
    p.drawLine(0, height()-55, width(), height()-55);

    if (mCorrect > 0) {
        QBrush brCorrect(QColor(78, 154, 6));
        p.setBrush(brCorrect);
        float t = oneBarHeight * mCorrect;
        p.setPen(Qt::white);
        p.drawRect(84, 395 - t, barWidth, t);
    }
    p.setPen(Qt::black);
    p.drawText(QRect(58, 405, 100, 20), Qt::AlignHCenter, tr("Correct"));

    if (mUncorrect > 0) {
        QBrush brUncorrect(QColor(239, 41, 41));
        p.setBrush(brUncorrect);
        float t = oneBarHeight * mUncorrect;
        p.setPen(Qt::white);
        p.drawRect(184, 395-t, barWidth, t);
    }
    p.setPen(Qt::black);
    p.drawText(QRect(160, 405, 100, 20), Qt::AlignHCenter, tr("Uncorrect"));
}

QSize BarsWidget::sizeHint() const {
    return QSize(345, 450);
}

QSize BarsWidget::minimumSizeHint() const {
    return QSize(345, 450);
}
