#ifndef FORMAUTH_H
#define FORMAUTH_H

#include <QWidget>

namespace Ui {
class FormAuth;
}

class FormAuth : public QWidget
{
    Q_OBJECT

public:
    explicit FormAuth(QWidget *parent = nullptr);
    ~FormAuth();

signals:
    void onAuth(QString login, QString pass);

private slots:
    void on_btLogin_clicked();

protected:
    void keyReleaseEvent(QKeyEvent *event);

private:
    Ui::FormAuth *ui;
};

#endif // FORMAUTH_H
