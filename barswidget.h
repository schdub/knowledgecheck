#ifndef BARSWIDGET_H
#define BARSWIDGET_H

#include <QWidget>

class BarsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit BarsWidget(QWidget *parent = nullptr);

    void onContent(int correct, int uncorrect) {
        mCorrect = correct;
        mUncorrect = uncorrect;
    }

signals:

protected:
    void paintEvent(QPaintEvent *event);
    QSize sizeHint() const;
    QSize minimumSizeHint() const;

private:
    int mCorrect;
    int mUncorrect;
};

#endif // BARSWIDGET_H
