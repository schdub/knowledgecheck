#ifndef FORMTEST_H
#define FORMTEST_H

#include <QWidget>
#include <QJsonDocument>
#include <QJsonArray>
#include <QTimer>
#include <QTime>

namespace Ui {
class FormTest;
}

class FormTest : public QWidget {
    Q_OBJECT

public:
    explicit FormTest(QWidget *parent = nullptr);
    ~FormTest();

    QJsonArray answers() const {
        return mJsonArr;
    }

signals:
    void onAnswer();
    void onNext();
    void onPrev();
    void onQuit();
    void questionClicked(int index);

public slots:
    void onTestStart(int questionCount, int testDuration);
    void onContent(int index, const QJsonDocument &questions, const QJsonArray &answer);
    void slotQuestionClicked(int index);

private slots:
    void on_btAnswer_clicked();
    void on_btPrev_clicked();
    void on_btNext_clicked();
    void on_btSubmit_clicked();

    void onTimer();

private:
    Ui::FormTest *ui;

    QVector<QWidget*> mWidgets;
    QJsonArray mJsonArr;
    QTimer mTimer;
    QTime mCountdown;
    int mQuestionsCount;
    int mTestDuration;
};

#endif // FORMTEST_H
