#ifndef FORMRESULT_H
#define FORMRESULT_H

#include <QWidget>
#include <QJsonObject>

namespace Ui {
class FormResult;
}

class FormResult : public QWidget
{
    Q_OBJECT

public:
    explicit FormResult(QWidget *parent = nullptr);
    ~FormResult();

    void onContent(const QJsonObject & json);

signals:
    void onNext();

private:
    Ui::FormResult *ui;
};

#endif // FORMRESULT_H
