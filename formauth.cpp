#include "formauth.h"
#include "ui_formauth.h"

#include <QKeyEvent>

FormAuth::FormAuth(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::FormAuth)
{
    ui->setupUi(this);
}

FormAuth::~FormAuth() {
    delete ui;
}

void FormAuth::on_btLogin_clicked() {
    QString login;
    QString password;

    login = ui->edLogin->text();
    password = ui->edPassword->text();

    login = login.simplified();
    password = password.simplified();

    if (!login.isEmpty() && !password.isEmpty()) {
        emit onAuth(login, password);
    }
}

void FormAuth::keyReleaseEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Return) {
        if (focusWidget() == ui->edLogin) {
            ui->edPassword->setFocus();
        } else if (focusWidget() == ui->edPassword) {
            ui->btLogin->click();
        }
    }
}
