#ifndef FORMSELECTION_H
#define FORMSELECTION_H

#include <QWidget>
#include <QByteArray>
#include <QJsonArray>

#include <QTableWidgetItem>

namespace Ui {
class FormSelection;
}

class FormSelection : public QWidget
{
    Q_OBJECT

public:
    explicit FormSelection(QWidget *parent = nullptr);
    ~FormSelection();

    void onContent(const QJsonArray &a);

signals:
    void onSelected(int index);

private slots:
    void on_tableWidget_itemDoubleClicked(QTableWidgetItem *item);

    void on_pushButton_clicked();

private:
    Ui::FormSelection *ui;
};

#endif // FORMSELECTION_H
