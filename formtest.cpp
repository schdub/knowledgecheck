#include "formtest.h"
#include "ui_formtest.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QRadioButton>
#include <QCheckBox>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QDateTime>

#include <QDebug>

#define TRACE() qDebug() << __FUNCTION__

// ///////////////////////////////////////////////////////////////////////// //

FormTest::FormTest(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::FormTest)
{
    ui->setupUi(this);
    connect(ui->widgetMenu, SIGNAL(questionClicked(int)), this, SLOT(slotQuestionClicked(int)));
}

// ///////////////////////////////////////////////////////////////////////// //

FormTest::~FormTest() {
    delete ui;
}

// ///////////////////////////////////////////////////////////////////////// //

void FormTest::onTimer() {
    mCountdown = mCountdown.addSecs(-1);
    ui->labTime->setText( mCountdown.toString("mm:ss") );
}

// ///////////////////////////////////////////////////////////////////////// //

void FormTest::onTestStart(int questionCount, int testDuration) {
    mQuestionsCount = questionCount;
    mTestDuration = testDuration;
    ui->widgetMenu->setTotalCount(questionCount);

    mCountdown = QTime(0, mTestDuration, 0);

    connect( &mTimer, SIGNAL(timeout()), this, SLOT(onTimer()) );
    mTimer.setInterval(1000);
    mTimer.start();
}

// ///////////////////////////////////////////////////////////////////////// //

void FormTest::onContent(int index, const QJsonDocument & questions, const QJsonArray & answer) {

    TRACE() << index;

    if ( !questions.isArray() || questions.isEmpty() ) {
        qWarning() << "questions is invalid.";
        return;
    }

    ui->widgetMenu->setCurrent(index);

    const QJsonObject & question = questions.array()[index].toObject();

    ui->labText->setText(question["Text"].toString());
    int type = question["Type"].toInt();
    const QString & strPic = question["Picture"].toString();

    if (type < 0 || type > 2) {
        // FIX: invalid type
        qWarning() << "type can't be" << type;
        return;
    }

    // loading picture for the question
    ui->labPicture->setVisible(false);
    if (!strPic.isEmpty()) {
        QPixmap pix;
        if (pix.loadFromData(QByteArray::fromBase64(strPic.toUtf8()))) {
            ui->labPicture->setPixmap(pix);
            ui->labPicture->setVisible(true);
        }
    }

    delete ui->frameOptions->layout();
    qDeleteAll(mWidgets);
    mWidgets.clear();

    QVBoxLayout * lay = new QVBoxLayout;
    if (type == 2) {
        QLineEdit * w = new QLineEdit;
        if (answer.count() == 1)
            w->setText( answer[0].toString() );
        lay->addWidget(w);
        mWidgets.push_back(w);
    } else {
        const QJsonArray & array = question["Items"].toArray();
        if (type == 0) {
            for (int i = 0; i < array.count(); ++i) {
                QCheckBox * w = new QCheckBox(array.at(i).toString()) ;
                if (answer[i].toInt() == 1)
                    w->setChecked(true);
                mWidgets.push_back(w);
                lay->addWidget(w);
            }
        } else {
            for (int i = 0; i < array.count(); ++i) {
                QRadioButton * w = new QRadioButton(array.at(i).toString()) ;
                if (answer[i].toInt() == 1)
                    w->setChecked(true);
                mWidgets.push_back(w);
                lay->addWidget(w);
            }
        }
    }

    ui->frameOptions->setLayout(lay);
    ui->frameOptions->update();
    ui->frameOptions->updateGeometry();

    update();
}

// ///////////////////////////////////////////////////////////////////////// //

void FormTest::on_btAnswer_clicked() {

    bool somethingIsActuallyInputed = false;

    QJsonArray arr;
    if (mWidgets.count() == 1) {
        QLineEdit * le = qobject_cast<QLineEdit*>(mWidgets[0]);
        QString s = le->text().simplified();
        arr.push_back(s);

        somethingIsActuallyInputed = (s.size() > 0);
    } else {
        QAbstractButton * bt = NULL;
        bt = qobject_cast<QAbstractButton*>(mWidgets[0]);

        if (!bt) {
            qWarning() << "can't cast to QAbstractButton.";
            return;
        } else {
            for (int i = 0; i < mWidgets.count(); ++i) {
                bt = qobject_cast<QAbstractButton*>(mWidgets[i]);
                arr.push_back(bt->isChecked() ? 1 : 0);
                somethingIsActuallyInputed |= bt->isChecked();
            }
        }
    }

    mJsonArr = arr;

    if (somethingIsActuallyInputed) {
        ui->widgetMenu->setAsAnswered();
        emit onAnswer();
    } else {
        emit onNext();
    }

}

// ///////////////////////////////////////////////////////////////////////// //

void FormTest::on_btPrev_clicked() {
    emit onPrev();
}

// ///////////////////////////////////////////////////////////////////////// //

void FormTest::on_btNext_clicked() {
    emit onNext();
}

// ///////////////////////////////////////////////////////////////////////// //

void FormTest::on_btSubmit_clicked() {
    emit onQuit();
}

// ///////////////////////////////////////////////////////////////////////// //

void FormTest::slotQuestionClicked(int index) {
    emit questionClicked(index);
}

// ///////////////////////////////////////////////////////////////////////// //
