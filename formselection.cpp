#include "formselection.h"
#include "ui_formselection.h"

#include <QTableWidgetItem>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include <QDebug>

#define TRACE() qDebug() << __FUNCTION__

// ///////////////////////////////////////////////////////////////////////// //

FormSelection::FormSelection(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormSelection)
{
    ui->setupUi(this);

    ui->tableWidget->setColumnWidth(0, 600);
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);
}

// ///////////////////////////////////////////////////////////////////////// //

FormSelection::~FormSelection() {
    delete ui;
}

// ///////////////////////////////////////////////////////////////////////// //

void FormSelection::onContent(const QJsonArray & a) {
    TRACE();
    ui->tableWidget->setRowCount( a.size() );
    QTableWidgetItem * item = nullptr;
    for (int i = 0; i < a.size(); ++i) {
        const QJsonObject & o = a.at(i).toObject();
        int id = o["id"].toInt();

        item = new QTableWidgetItem( o["name"].toString() );
        item->setData(Qt::UserRole, id);
        ui->tableWidget->setItem(i, 0, item);
        item = new QTableWidgetItem( QString::number(o["duration"].toInt()) );
        item->setData(Qt::UserRole, id);
        ui->tableWidget->setItem(i, 1, item);
    }
}

// ///////////////////////////////////////////////////////////////////////// //

void FormSelection::on_tableWidget_itemDoubleClicked(QTableWidgetItem *item) {
    TRACE();

    if (item) {
        emit onSelected(item->data(Qt::UserRole).toInt());
    }
}

// ///////////////////////////////////////////////////////////////////////// //

void FormSelection::on_pushButton_clicked() {
    auto si = ui->tableWidget->selectedItems();
    if (si.size() > 0) {
        emit onSelected((*(si.begin()))->data(Qt::UserRole).toInt());
    }
}

// ///////////////////////////////////////////////////////////////////////// //
