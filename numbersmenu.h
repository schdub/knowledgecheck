#ifndef NUMBERSMENU_H
#define NUMBERSMENU_H

#include <QWidget>
#include <QVector>

class NumbersMenu : public QWidget {
    Q_OBJECT

public:
    explicit NumbersMenu(QWidget *parent = nullptr);

    void setTotalCount(int count);
    void setCurrent(int current);
    void setAsAnswered();

signals:
    void questionClicked(int index);

protected:
    void paintEvent(QPaintEvent *event);
    QSize sizeHint() const;
    QSize minimumSizeHint() const;
    void mousePressEvent(QMouseEvent *event);

private:
    int mCurrent;
    QVector<int> mNumbers;
};

#endif // NUMBERSMENU_H
