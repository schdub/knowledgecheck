#include "numbersmenu.h"

#include <QPainter>
#include <QMouseEvent>
#include <QDebug>

// ///////////////////////////////////////////////////////////////////////// //

NumbersMenu::NumbersMenu(QWidget *parent) : QWidget(parent), mCurrent(-1)
{}

// ///////////////////////////////////////////////////////////////////////// //

void NumbersMenu::setTotalCount(int count) {
    mNumbers.resize(count);
    for (int i = 0; i < count; ++i)
        mNumbers[i] = -1;
}

// ///////////////////////////////////////////////////////////////////////// //

void NumbersMenu::setCurrent(int current) {
    mCurrent = current;
}

void NumbersMenu::setAsAnswered() {
    Q_ASSERT(mCurrent >= 0);
    Q_ASSERT(mCurrent < mNumbers.size());
    mNumbers[mCurrent] = 1;
}

// ///////////////////////////////////////////////////////////////////////// //

void NumbersMenu::mousePressEvent(QMouseEvent *event) {
    float oneWidth = width() / mNumbers.count();
    int number = event->pos().x() / oneWidth;
    qDebug() << number << "was pressed";
    if (number >= 0 && number < mNumbers.size()) {
        emit questionClicked(number);
    }
}

// ///////////////////////////////////////////////////////////////////////// //

void NumbersMenu::paintEvent(QPaintEvent *event) {
    Q_UNUSED(event);

    QPainter p(this);
    QBrush brAnswered(QColor("#4d6d9a"));
    QBrush brUnanswered(Qt::gray);
    QBrush brCurrent(Qt::darkGray);
    p.setPen(Qt::white);

    float x = 0;
    float oneWidth = width() / mNumbers.count();
    for (int counter = 0; counter < mNumbers.size(); ++counter) {
        if (counter == mCurrent) {
            p.setBrush(brCurrent);
        } else {
            p.setBrush((mNumbers[counter] == 1) ? brAnswered : brUnanswered);
        }

        QRectF rect(x+1, 0+1, oneWidth-1, height()-1);
        p.drawRect(rect);
        p.drawText(rect, Qt::AlignCenter, QString::number(counter+1));

        x += oneWidth;
    }
}

// ///////////////////////////////////////////////////////////////////////// //

QSize NumbersMenu::sizeHint() const {
    return QSize(0, 50);
}

// ///////////////////////////////////////////////////////////////////////// //

QSize NumbersMenu::minimumSizeHint() const {
    return QSize(0, 50);
}

// ///////////////////////////////////////////////////////////////////////// //
