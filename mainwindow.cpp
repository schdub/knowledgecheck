#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "formauth.h"
#include "formtest.h"
#include "formresult.h"
#include "formselection.h"

#include <QScreen>

#include <QMessageBox>
#include <QByteArray>
#include <QDebug>

#include <QTimer>
#include <QCloseEvent>
#include <QDateTime>

#include <QJsonDocument>
#include <QCryptographicHash>

// https://kubzan.ru/vacancy/detail/92839f8e-82fd-4fb6-b6e3-fbffc702ed2a/
// f1a196
// d94c3e
// db978d
// 212121

static const QString sAppName = QObject::tr("Knowledge Check");
static const QString sConnectHost = "localhost";
static const quint16 sConnectPort = 45329;
static const int sConnectionTimeout = 500;

#define TRACE() qDebug() << __FUNCTION__

// ///////////////////////////////////////////////////////////////////////// //

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , mCurrentQuestion(-1)
    , mTotalQuestions(0)
    , mStatus(statusLogin)
    , mTimer(nullptr)
    , mTestDuration(0)
    , mTesting(false)
    , mConnected(false)
    , ui(new Ui::MainWindow)
{
    connect(&mSocket, SIGNAL(disconnected()), this, SLOT(slotDisconnect()));
    connect(&mSocket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(&mSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(displayError(QAbstractSocket::SocketError)));

    ui->setupUi(this);
    move(QGuiApplication::screens().at(0)->geometry().center() - frameGeometry().center());

    //setStyleSheet( "background-color: rgb(241, 161, 150);" );
    setWindowTitle( "KCheck" );
    setWindowIcon( QIcon(":/img/arrow.png") );

    FormAuth * frmAuth = new FormAuth;
    connect( frmAuth, SIGNAL(onAuth(QString,QString)), this, SLOT(onAuth(QString,QString)) );
    setCentralWidget( frmAuth );
}

// ///////////////////////////////////////////////////////////////////////// //

MainWindow::~MainWindow() {
    delete ui;
}

// ///////////////////////////////////////////////////////////////////////// //


void MainWindow::closeEvent(QCloseEvent *event) {
    if ( !mTesting || QMessageBox::Yes ==
        QMessageBox::question(this, sAppName, tr("Test in progress."))) {
        event->accept();
    } else {
        event->ignore();
    }
}

// ///////////////////////////////////////////////////////////////////////// //

void MainWindow::slotDisconnect() {
    TRACE();
    mConnected = false;
}

// ///////////////////////////////////////////////////////////////////////// //

void MainWindow::slotOnEndTimer() {
    TRACE();

    mTesting = false;
    mStatus = statusResult;

    if (mTimer)
        mTimer->stop();

    QString r = "{ \"ts\": \"" + QDateTime::currentDateTimeUtc().toString() + "\" }";
    mSocket.write(r.toUtf8());
    mSocket.flush();
}

// ///////////////////////////////////////////////////////////////////////// //

void MainWindow::readyRead() {
    TRACE();
    QByteArray buff = mSocket.readAll();
    qDebug() << buff;
    mBuff += buff;

    if (buff.isEmpty()) return;

    QJsonDocument json = QJsonDocument::fromJson(mBuff);
    if (!json.isObject() && !json.isArray()) {
        qDebug() << "can't parse";
        return;
    }

    mBuff.clear();

    switch (mStatus) {

    case statusLogin: {
        if (json.isObject()) {
            mStatus = statusFail;
            QMessageBox::information(this, sAppName, json["e"].toString());
        } else if (json.isArray()) {
            mStatus = statusTestPick;
            FormSelection * frmSel = new FormSelection;
            connect( frmSel, SIGNAL(onSelected(int)), this, SLOT(onSelected(int)) );
            frmSel->onContent( json.array() );
            setCentralWidget( frmSel );
        }
        break;
    } // case statusLogin:

    case statusTestPick: {
        if (json.isObject()) {
            if (!json["e"].isUndefined()) {
                mStatus = statusFail;
                QMessageBox::information(this, sAppName, json["e"].toString());
            } else {
                mTestDuration = json["dur"].toInt();
                const QJsonArray & arr = json["a"].toArray();

                mStatus = statusTesting;
                mQuestions.setArray(arr);
                mCurrentQuestion = 0;
                mTotalQuestions = arr.size();
                mAnswers.resize(mTotalQuestions);

                FormTest * frmTest = new FormTest;
                connect( frmTest, SIGNAL(onAnswer()), this, SLOT(onAnswer()) );
                connect( frmTest, SIGNAL(onNext()), this, SLOT(onNext()) );
                connect( frmTest, SIGNAL(onPrev()), this, SLOT(onPrev()) );
                connect( frmTest, SIGNAL(onQuit()), this, SLOT(slotOnEndTimer()) );
                connect( frmTest, SIGNAL(questionClicked(int)), this, SLOT(slotQuestionClicked(int)) );
                frmTest->onTestStart(mTotalQuestions, mTestDuration);
                frmTest->onContent(mCurrentQuestion, mQuestions, mAnswers[mCurrentQuestion]);
                setCentralWidget( frmTest );

                mTesting = true;

                mTimer = new QTimer(this);
                connect( mTimer, SIGNAL(timeout()), this, SLOT(slotOnEndTimer()) );
                mTimer->setSingleShot(true);
                mTimer->setInterval( mTestDuration * 1000 * 60 );
                mTimer->start();
            }
        }
        break;
    } // case statusTestPick:

    case statusTesting: {
        break;
    } // case statusTesting:

    case statusResult: {
        if (json.isObject()) {
            if (!json["e"].isUndefined()) {
                mStatus = statusFail;
                QMessageBox::information(this, sAppName, json["e"].toString());
            } else {
                FormResult * frmResult = new FormResult;
                connect( frmResult, SIGNAL(onNext()), this, SLOT(onNext()) );
                frmResult->onContent(json.object());
                setCentralWidget( frmResult );
            }
        }
        break;
    } // case statusResult:

    default:
        break;
    } // switch

}

// ///////////////////////////////////////////////////////////////////////// //

void MainWindow::displayError(QAbstractSocket::SocketError socketError) {
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, sAppName,
                                 tr("The host was not found. Please check the "
                                    "host name and port settings."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, sAppName,
                                 tr("The connection was refused by the peer. "
                                    "Make sure the server is running, "
                                    "and check that the host name and port "
                                    "settings are correct."));
        break;
    default:
        QMessageBox::information(this, sAppName,
                                 tr("The following error occurred: %1.")
                                 .arg(mSocket.errorString()));
    }
}

// ///////////////////////////////////////////////////////////////////////// //

void MainWindow::onAuth(QString login, QString pass) {
    TRACE() << login << pass;

    pass = QCryptographicHash::hash(pass.toUtf8(),QCryptographicHash::Md5).toHex();
    mCredentials = (QString("{\"l\":\"%1\", \"p\":\"%2\"}").arg(login).arg(pass)).toUtf8();

    if (!mConnected) {
        mSocket.connectToHost(sConnectHost, sConnectPort);
        if (!mSocket.waitForConnected(sConnectionTimeout)) {
            mConnected = false;
            mStatus = statusFail;
        } else {
            mConnected = true;
            mStatus = statusLogin;
            mSocket.write(mCredentials);
            mSocket.flush();
        }
    }
}

// ///////////////////////////////////////////////////////////////////////// //

void MainWindow::onPrev() {
    TRACE();

    FormTest * frmTest = qobject_cast<FormTest*>(centralWidget());
    Q_ASSERT(frmTest);

    if (mCurrentQuestion == 0) {
        mCurrentQuestion = mTotalQuestions-1;
    } else {
        mCurrentQuestion -= 1;
    }

    frmTest->onContent(mCurrentQuestion, mQuestions, mAnswers[mCurrentQuestion]);
}

// ///////////////////////////////////////////////////////////////////////// //

void MainWindow::onNext() {
    TRACE();

    FormTest * frmTest = qobject_cast<FormTest*>(centralWidget());
    Q_ASSERT(frmTest);

    if (mCurrentQuestion == mTotalQuestions-1) {
        mCurrentQuestion = 0;
    } else {
        mCurrentQuestion += 1;
    }

    frmTest->onContent(mCurrentQuestion, mQuestions, mAnswers[mCurrentQuestion]);
}

// ///////////////////////////////////////////////////////////////////////// //

void MainWindow::onAnswer() {
    TRACE() << mCurrentQuestion;

    Q_ASSERT( mCurrentQuestion > -1);
    Q_ASSERT( mCurrentQuestion < mAnswers.size());
    Q_ASSERT( mQuestions.array().size() == mAnswers.size());

    FormTest * frmTest = qobject_cast<FormTest*>(centralWidget());
    Q_ASSERT(frmTest);

    // отправим в сокет
    QJsonObject json;
    json["i"] = mCurrentQuestion;
    json["a"] = frmTest->answers();
    QByteArray b = QJsonDocument(json).toJson();
    mSocket.write(b);

    // сохраним
    mAnswers[mCurrentQuestion] = frmTest->answers();

    if (mCurrentQuestion == mTotalQuestions-1) {
        mCurrentQuestion = 0;
    } else {
        mCurrentQuestion += 1;
    }

    frmTest->onContent(mCurrentQuestion, mQuestions, mAnswers[mCurrentQuestion]);
}

// ///////////////////////////////////////////////////////////////////////// //

void MainWindow::onSelected(int index) {
    TRACE() << index;

    QString r = QString("{\"id\":%1}").arg(index);
    mSocket.write(r.toUtf8());
    mSocket.flush();
}

// ///////////////////////////////////////////////////////////////////////// //

void MainWindow::slotQuestionClicked(int index) {
    TRACE() << index;

    FormTest * frmTest = qobject_cast<FormTest*>(centralWidget());
    Q_ASSERT(frmTest);

    if (index < 0 || index >= mTotalQuestions) {
        qDebug() << "invalid index";
        return;
    }

    mCurrentQuestion = index;
    frmTest->onContent(mCurrentQuestion, mQuestions, mAnswers[mCurrentQuestion]);
}

// ///////////////////////////////////////////////////////////////////////// //
