#include "formresult.h"
#include "ui_formresult.h"


// ///////////////////////////////////////////////////////////////////////// //

FormResult::FormResult(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::FormResult)
{
    ui->setupUi(this);
}

// ///////////////////////////////////////////////////////////////////////// //

FormResult::~FormResult() {
    delete ui;
}

// ///////////////////////////////////////////////////////////////////////// //

void FormResult::onContent(const QJsonObject & obj) {

    int correct = obj["correct"].toInt();
    int incorrect = obj["incorrect"].toInt();
    int unanswered = obj["unanswered"].toInt();
    int grade = obj["grade"].toInt();
    float total = correct + incorrect + unanswered;

    ui->widgetBars->onContent(correct, incorrect);

    ui->prCorrect->setMaximum(total);
    ui->prIncorrect->setMaximum(total);
    ui->prUnanswered->setMaximum(total);

    ui->prCorrect->setValue(correct);
    ui->prIncorrect->setValue(incorrect);
    ui->prUnanswered->setValue(unanswered);

    QString strScore;
    QString strColor;

    if (grade == 2) {
        strScore = tr("Excellent");
        strColor = "4e9a06";
    } else if (grade == 1)  {
        strScore = tr("Good");
        strColor = "07b406";
    } else {
        strScore = tr("Not Bad");
        strColor = "ff0f0f";
    }

    ui->lab->setText(strScore);
    ui->lab->setStyleSheet(QString("QLabel{color: #%1 ;}").arg(strColor));
}

// ///////////////////////////////////////////////////////////////////////// //
